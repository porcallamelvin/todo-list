import { ref } from "vue";
import { MyTasks, FinishedTasks, indexToDelete, indexToEdit } from './Tasks';
import { ToggleMainDialogState } from '../../../composables/Triggers.js';
import MainDialog from '../../../components/MainDialog.vue';
import DeleteTask from "../components/DeleteTask.vue";
import { useQuasar } from 'quasar'


export default{
  components:{
    MainDialog,
    DeleteTask,
  },

  setup(){
    const $q = useQuasar()

  const doneTaskDialog = ref(false);


  const moveTaskBackToInProgress = (index) => {
    const checkArray = FinishedTasks.value[index].checkArray;
    const allChecked = checkArray.every(val => val);
    if (!allChecked) {
      console.log("all are checked");
      FinishedTasks.value[index].toggleExpansion = false;
      MyTasks.value.push(FinishedTasks.value[index]);
      FinishedTasks.value.splice(index, 1);
      doneTaskDialog.value = true;
      showNotif();
    }
  }
  const removeEmptyFirstArray = () => {
    if (MyTasks.value[0].taskTitle === null) {
      MyTasks.value.shift();
      FinishedTasks.value.shift();
      indexToEdit.value.shift();
    }

  }
  removeEmptyFirstArray();

  const editTodo = (index) => {
    indexToEdit.value = MyTasks.value[index];
    indexToDelete.value = index.value;
  }

  const deleteTodo = (index) => {
    indexToDelete.value = index;
    ToggleMainDialogState();
  }
  const showNotif = () => {
    $q.notify({
      message: 'Successfully Completed Task!',
      caption: 'To-do List  has been  Added to Done section  successfully!',
      color: 'teal',
      position: 'bottom-right'
    });
  };



  const checkBoxChecker = (index) => {
    console.log("a check box clicked")
    const checkArray = MyTasks.value[index].checkArray;
    const allChecked = checkArray.every(val => val);
    if (allChecked) {
      console.log("all are checked");
      MyTasks.value[index].toggleExpansion = false;
      FinishedTasks.value.push(MyTasks.value[index]);
      MyTasks.value.splice(index, 1);
      doneTaskDialog.value = true;
      showNotif();
    }
  }

    return{
      MyTasks,
      FinishedTasks,
      ToggleMainDialogState,
      checkBoxChecker,
      editTodo,
      deleteTodo,
      doneTaskDialog,
      showNotif,
      moveTaskBackToInProgress,

    };
  },
}
