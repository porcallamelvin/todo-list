import 'bootstrap-icons/font/bootstrap-icons.css';
import { ref } from "vue";
import { MyTasks } from './Tasks';
import { date } from 'quasar';
import { useQuasar } from 'quasar'

export default {

  setup() {

    let form = ref([{taskTitle: "", taskName: [], taskTime: [] , toggleCardTime: false, taskDate: "", toggleExpansion: false,}]);
    const saveDialog = ref(false);
    const $q = useQuasar()

    const addKeyResult = () => {
      form.value.push({});
    };

    const showNotif = () => {
      $q.notify({
        message: 'Successfully Completed Task!',
        caption: 'To-do List  has been  Added to Done section  successfully!',
        color: 'teal',
        position: 'bottom-right'
      });
    };

    const removeKeyResult = (index) => {
      if (index > -1) {
        form.value.splice(index, 1);
      }
    };

    const saveTask = () => {
      const tasks = {
        taskTitle: form.value.taskTitle,
        taskName: [],
        taskTime: [],
        checkArray: [],
        taskDate: new Date().toLocaleDateString('en-US', { month: 'long', day: 'numeric', year: 'numeric' }),
        toggleExpansion: false,
      }

      for (let i = 0 ; i < form.value.length; i++){
          tasks.taskName.push(form.value[i].taskName);
          tasks.taskTime.push(form.value[i].taskTime);
          tasks.checkArray.push(false);
      }

      MyTasks.value.push(tasks);
      saveDialog.value = true;
      showNotif();
    };

    return {
      form,
      addKeyResult,
      removeKeyResult,
      saveTask,
      MyTasks,
      saveDialog,
      showNotif,


    };
  },

  methods: {
    toggleCard(index) {
      this.form[index].toggleCardTime = !this.form[index].toggleCardTime;
    },
    updateTime(newTime) {
      this.selectedTime = newTime;
    },

  },

  data() {
    return {
      selectedTime: null,
    };
  },
};
