import 'bootstrap-icons/font/bootstrap-icons.css';
import { ref } from "vue";
import { MyTasks, indexToEdit, indexToDelete } from './Tasks';
import { date } from 'quasar';
import { useQuasar } from 'quasar'

export default {

  setup() {
    let form = ref ([{taskTitle: '', taskName: [], taskTime: [], checkArray: [], toggleExpansion: false,}])
    form.value.taskTitle = indexToEdit.value.taskTitle;

    const saveDialog = ref(false);
    const $q = useQuasar()

    const addDataToForm = () => {
      for (let i = 0; i < indexToEdit.value.taskName.length; i++){
        form.value[i].taskName = indexToEdit.value.taskName[i];
        form.value[i].taskTime = indexToEdit.value.taskTime[i];
        form.value.push({});
      }
      form.value.splice(indexToEdit.value.taskName.length, 1);
    }
    addDataToForm();

    const showNotif = () => {
      $q.notify({
        message: 'Successfully Updated!',
        caption: 'New To-do List  has been  Updated successfully!',
        color: 'teal',
        position: 'bottom-right'
      });
    };


    const addKeyResult = () => {
      form.value.push({});
    };

    const removeKeyResult = (index) => {
      if (index > -1) {
        form.value.splice(index, 1);
      }
    };

    const saveTask = () => {
      const tasks = {
        taskTitle: form.value.taskTitle,
        taskName: [],
        taskTime: [],
        checkArray: [],
        taskDate: new Date().toLocaleDateString('en-US', { month: 'long', day: 'numeric', year: 'numeric' }),
        toggleExpansion: false,
      }

      for (let i = 0 ; i < form.value.length; i++){
          tasks.taskName.push(form.value[i].taskName);
          console.log(form.value[i].taskName);
          tasks.taskTime.push(form.value[i].taskTime);
          tasks.checkArray.push(false);
      }

      MyTasks.value.splice(indexToDelete, 1);
      MyTasks.value.push(tasks);
      console.log(MyTasks[indexToDelete]);
      saveDialog.value = true;
      showNotif();
    };

    return {
      form,
      addKeyResult,
      removeKeyResult,
      saveTask,
      MyTasks,
      saveDialog,
      indexToEdit,
      showNotif,
    };
  },

  methods: {
    toggleCard(index) {
      this.form[index].toggleCardTime = !this.form[index].toggleCardTime;
    },

    updateTime(newTime) {
      this.selectedTime = newTime;
    },

  },

  data() {
    return {
      selectedTime: null,
    };
  },
};
